package com.flashitech.securex;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TextView;

import com.flashitech.securex.model.App;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.flashitech.securex.app.Constant.EXTRA_APP;

public class RiskActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView textViewRisk, textViewPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risk);
        initObjects();
        initToolbar();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewRisk = findViewById(R.id.txt_risk);
        textViewPermissions = findViewById(R.id.txt_permissions);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            App app = bundle.getParcelable(EXTRA_APP);
            if (app != null) {
                if (getSupportActionBar() != null) getSupportActionBar().setTitle(app.getName());
                try {
                    PackageInfo packageInfo = getPackageManager().getPackageInfo(app.getPackageName(), PackageManager.GET_PERMISSIONS);
                    String[] requestedPermissions = packageInfo.requestedPermissions;
                    if (requestedPermissions != null) {
                        textViewPermissions.setText(TextUtils.join("\n\n", requestedPermissions));
                    } else {
                        textViewPermissions.setText(getString(R.string.error_na_permissions));
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
