package com.flashitech.securex.app;

import android.app.Application;

import com.flashitech.securex.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by gladwinbobby on 06/11/17
 */

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Signika-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
