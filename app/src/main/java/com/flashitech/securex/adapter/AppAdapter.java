package com.flashitech.securex.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flashitech.securex.R;
import com.flashitech.securex.model.App;

import java.util.List;

/**
 * Created by gladwinbobby on 06/11/17
 */

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.AppHolder> {

    private Context context;
    private List<App> appList;
    private AppClickListener listener;

    public AppAdapter(Context context, List<App> appList, AppClickListener listener) {
        this.context = context;
        this.appList = appList;
        this.listener = listener;
    }

    @Override
    public AppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AppHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(AppHolder holder, int position) {
        App app = appList.get(position);
        holder.imageView.setImageDrawable(app.getIcon());
        holder.textViewName.setText(app.getName());
        holder.textViewPackageName.setText(app.getPackageName());
    }

    @Override
    public int getItemCount() {
        return appList.size();
    }

    public interface AppClickListener {
        void onAppClick(App app);
    }

    class AppHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView textViewName, textViewPackageName;
        AppClickListener listener;

        AppHolder(View itemView, AppClickListener listener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewPackageName = itemView.findViewById(R.id.txt_package_name);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                listener.onAppClick(appList.get(getLayoutPosition()));
            }
        }
    }
}
