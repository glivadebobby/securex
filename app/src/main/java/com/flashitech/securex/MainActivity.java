package com.flashitech.securex;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.flashitech.securex.adapter.AppAdapter;
import com.flashitech.securex.model.App;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.flashitech.securex.app.Constant.EXTRA_APP;

public class MainActivity extends AppCompatActivity implements AppAdapter.AppClickListener {

    private Toolbar toolbar;
    private RecyclerView viewApps;
    private List<App> appList;
    private AppAdapter appAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initToolbar();
        initRecyclerView();
        loadApps();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onAppClick(App app) {
        Intent intent = new Intent(this, RiskActivity.class);
        intent.putExtra(EXTRA_APP, app);
        startActivity(intent);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        viewApps = findViewById(R.id.apps);

        appList = new ArrayList<>();
        appAdapter = new AppAdapter(this, appList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        viewApps.setLayoutManager(new GridLayoutManager(this, 2));
        viewApps.setAdapter(appAdapter);
    }

    private void loadApps() {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo applicationInfo : packages) {
            appList.add(new App(applicationInfo.loadIcon(getPackageManager()),
                    applicationInfo.loadLabel(getPackageManager()).toString(),
                    applicationInfo.packageName));
        }
        appAdapter.notifyDataSetChanged();
    }
}
